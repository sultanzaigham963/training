<?php
namespace Sultan\PluginsExercise\Plugin;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\Action\Action;
/**
 *
 * Class PluginClass
 */
class PluginClass
{
    private $logger;
    private $message;

    /**
     * PluginClass constructor.
     * @param LoggerInterface $logger
     */
    public function __construct( LoggerInterface $logger )
    {
        $this->logger = $logger;
    }
    /**
     *use afterdispatch it modify the result
     */
    public function afterdispatch(Action $subject,$result)
    {

        $fullname = $subject->getRequest()->getFullActionName();
        /**
         *assign values to  message variable
         */
        $this->message = "Full Name Logger Information: " . $fullname;
        /**
         *give values to logger
         */
        $this->logger->info($this->message);
        return $result;
    }
}




