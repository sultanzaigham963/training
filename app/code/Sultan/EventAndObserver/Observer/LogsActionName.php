<?php

namespace Sultan\EventAndObserver\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

/**
 * create a class logsactionname and we need a $request object

 *
 * Class LogsActionName
 */

class LogsActionName implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    /**
     * @var LoggerInterface \
     */

    private $logger;
    private $request;

    /**
     *call parent class constructor and assign value to this class $_logger
     */
    public function __construct( LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    /**
     *function with observer parameter execute with any action ocr
     */

    public function execute(Observer $observer)
    {
        $this->request = $observer->getEvent()->getRequest();
        $this->logger->info('Full action name Observer : ' . $this->request->getFullActionName());
    }
}
