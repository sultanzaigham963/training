<?php
use Magento\Framework\Component\ComponentRegistrar;
/**
 * @author Eguana Team
 * @copyriht Copyright (c) 2021 Eguana {http://eguanacommerce.com}
 * Created by PhpStorm
 * User: sultanzgm
 * Date: 6/10/21
 * Time: 10 :47 Am
 */
ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Sultan_EventAndObserver',
    __DIR__
);
