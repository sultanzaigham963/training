<?php
/**
 * @author Eguana Team
 * @copyriht Copyright (c) 2021 Eguana {http://eguanacommerce.com}
 * Created by PhpStorm
 * User: sultanzgm
 * Date: 6/10/21
 * Time: 10:54 AM
 */
namespace Sultan\HelloWorldCli\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class HelloWorld
 * This class adds a new command to the magento default commands
 * once run, the command prints hello world
 */
class HelloWorld extends Command
{
    /**
     * overriding configure method from Command Class
     *   Name of the command and its description
     */
    protected function configure()
    {
        $this->setName('print:hello:world');
        $this->setDescription('Demo command line');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Demo Hello World</info>');
    }
}
