<?php
/**
/**
 * @author Eguana Team
 * @copyriht Copyright (c) 2021 Eguana {http://eguanacommerce.com}
 * Created by PhpStorm
 * User: sultanzgm
 * Date: 6/9/21
 * Time: 5:47 PM
 */
namespace Sultan\ChangeCopyrightFooterDi\Block;
/**
 * add class to change copyright text
 *
 * Class Footer
 */
class Footer extends \Magento\Theme\Block\Html\Footer
{
    /**
     * @return \Magento\Framework\Phrase|string
     */

    public function getCopyright()
    {
        if (!$this->_copyright) {
            $this->_copyright = "Hello World !";
        }
        return __($this->_copyright);
    }
}
