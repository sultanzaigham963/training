<?php
/**
 * @author Eguana Team
 * @copyriht Copyright (c) 2021 Eguana {http://eguanacommerce.com}
 * Created by PhpStorm
 * User: sultanzgm
 * Date: 6/9/21
 * Time: 5:47 PM
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Sultan_ChangeCopyrightFooterDi',
    __DIR__
);
