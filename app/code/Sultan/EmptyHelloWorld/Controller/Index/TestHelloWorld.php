<?php
/**
 * @author Eguana Team
 * @copyriht Copyright (c) 2021 Eguana {http://eguanacommerce.com}
 * Created by PhpStorm
 * User: sultanzgm
 * Date: 6/9/21
 * Time: 5:47 PM
 */
namespace Sultan\EmptyHelloWorld\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

/**
 * Add class TestHelloWorld to show hello world at frontend
 * Class TestHelloWorld
 */
class TestHelloWorld extends Action
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * TestHelloWorld constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    /**
     * This method echo hello world
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        echo "Hello World";
        exit;
    }
}
